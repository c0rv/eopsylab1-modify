#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdio.h>
#define SEM_KEY 17122010	// a key to a set of semaphores (any number)
#define FOOD_LIMIT 1		// number of meals for each philosopher
#define EATING_TIME 1		// eating time
#define THINKING_TIME 1		// thinking time

void grab_forks(int left_fork_id);
void put_away_forks(int left_fork_id);
void lifecycle();
void eat(int meals_left);
void think();

char* philosophers_list[5] = {"Piotro","Ja","Milosz","Koziako","robak"};
int philosopher_id,semaphor_id,pid,status;

int main() {
	int i=0; // temporary loop counter
	semaphor_id = semget(SEM_KEY, 5, 0644 | IPC_CREAT); // 644 = rw- r-- r--
	if(semaphor_id == -1) //semaphore is not created properly
	{
		printf("error:unsucesfull allocation of semaphores\n");
		exit(1);
	}
	while(i <= 4) //initializing 5 semaphores to avoid furter errors
		semctl(semaphor_id, i++, SETVAL, 1); // it menages the semaphores/setting them
		i = 0;// resetting counter
	while(i <= 4) {
		pid = fork(); // saving number of process which we are evaluate
		if(pid == 0) {
			philosopher_id = i;
			lifecycle();// thinking->eating->leaving
			printf ("%s has left the table.\n",philosophers_list[philosopher_id]);
			return 0;
		}
		else if(pid < 0) // if proccess id is <0, it clearly does not exist so we have to erase everything connected
		{
			kill(-2,SIGTERM);
			printf("\nerror: can't create a processes\n");
			exit(1);
		}
		++i;
	}
	while(1) {
		pid = wait(&status);
		if(pid < 0)
			break;	// all semaphores are initalized, so we can move on
	}

	return 0;
}

void grab_forks(int left_fork_id) {
	// select right fork with id one less than id of the left fork
	int right_fork_id = left_fork_id-1;
	if(right_fork_id<0)
		right_fork_id=4;	// when the left fork is 0, we have to use right as 4, not -1
	printf("%s is going to use left fork no. %d and right fork no. %d.\n", philosophers_list[philosopher_id],left_fork_id, right_fork_id);	
	struct sembuf semaphor_as_a_fork[2] = {
		{right_fork_id,-1,0},
		{left_fork_id,-1,0}
	};			//sembuf struct got special values like sem_num; sem_op;sem_flg; which are used to operate on semaphore properly
	semop(semaphor_id, semaphor_as_a_fork, 2);
}

void put_away_forks(int left_fork_id) {
	int right_fork_id = left_fork_id-1;
	if(right_fork_id<0)
		right_fork_id=4;
	printf("%s puts away left fork no. %d and right fork no. %d.\n", philosophers_list[philosopher_id], left_fork_id, right_fork_id);
	struct sembuf semaphor_as_a_fork[2] = {
		{right_fork_id,1,0},
		{left_fork_id,1,0}
	};	// setting sembuf to default values
	semop(semaphor_id, semaphor_as_a_fork, 2);
}

void lifecycle() {
	printf("%s came to the table.\n", philosophers_list[philosopher_id]); 
	int no_of_meals = FOOD_LIMIT;
	char hungry = 0;
	while(no_of_meals) {
		if(hungry) {
			eat(--no_of_meals);
			hungry = 0;
		}
		else {
			think();
			hungry = 1;
		}
	}
}

void eat(int meals_left) {
	grab_forks(philosopher_id);
	printf("%s is eating\n", philosophers_list[philosopher_id]);
	sleep(EATING_TIME);
	printf("%s ate his %d meal",philosophers_list[philosopher_id],(FOOD_LIMIT-meals_left));
	put_away_forks(philosopher_id);
}

void think() {
	printf("%s is thinking\n", philosophers_list[philosopher_id]);
	sleep(THINKING_TIME);
}
