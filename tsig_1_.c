#include <stdio.h>
#include <signal.h>
#include <wait.h>
#include <stdlib.h>
#include <unistd.h>


#define NUM_CHILD 20
#define WITH_SIGNALS

#ifdef WITH_SIGNALS // section with signals[3]
	char interrupt_flag = 0; //  3b. set your own keyboard interrupt signal handler (symbol of this interrupt:SIGINT)

	void k_intterupt() 
	{
		printf("parent [%i]: The keyboard interrupt received.\n",getppid());  // 3c.if we interrupt with ctrl+c we get notification on global variable interrupt flag
		interrupt_flag = 1;
	}

	void kill_child() 
	{
		printf("child [%i]:[%i]: Termination of the process.", getppid(),getpid()); // 3childb print message of the termination fo this process
	}
	
#endif

int main(){
	pid_t child_pid,parent_pid;		// temp child and parent process PID 	
	int temp1,temp2;				// process status
	parent_pid=getppid();	

    struct sigaction sigrestore;        // declaring sigrestore there because we are using it
	sigrestore.sa_handler = SIG_DFL;	// not only once
	
	printf("parent[%i]: The parent is starting.\n",parent_pid);
	for (int i = 0; i < NUM_CHILD; ++i) {
	
		/*
		Force ignoring of all signals with the signal() (or sigaction()) but
		after that at once restore the default handler for SIGCHLD signal
		*/

		struct sigaction sigignore;
		sigignore.sa_handler = SIG_IGN;
		#ifdef WITH_SIGNALS
			for(int j = 0; j < NSIG; ++j)
			{ // NSIG - total numbers of signals defined
				sigaction(j,&sigignore,NULL); //  sets the disposition of sig to SIG_IGN
			}
				
				sigaction(SIGCHLD, &sigrestore, NULL);		
			struct sigaction siginterrupt; //	Set your own keyboard interrupt signal handler  
			siginterrupt.sa_handler = k_intterupt ;
			sigaction(SIGINT, &siginterrupt, NULL); // (symbol of this interrupt:  SIGINT)
		#endif



		if (!(child_pid = fork())){
			/*
			In the child process
			  a. set to ignore handling of the keyboard interrupt signal
			*/
			#ifdef WITH_SIGNALS
				sigaction(SIGINT, &sigignore, NULL);	// SIGINT default ends the procces after getting the information sigint sigignore
				struct sigaction sigTerm;
            	sigTerm.sa_handler = kill_child;
            	sigaction(SIGTERM, &sigTerm, NULL); //set handler for SIGTERM of child
			#endif


			printf("child [%i]:[%i]: I'm created.\n",parent_pid,getpid());
			sleep(10);
			printf("child [%i]:[%i]: I've completed execution.\n",parent_pid,getpid());
			exit(0);
			}
		else if (child_pid == -1){
			printf("parent [%i]: Couldn't create new child.\n",getppid());
			kill(-1,SIGTERM); // sigterm - correct end of a proccess
			exit(1);	// exit with error
	}
		sleep(1); // Insert one second delays between consecutive fork() calls.
  			
		/*
		Between the two consequtive creations of new processes 
		check the mark which may be set by the keyboard interrupt handler. 
		If the mark is set the parent process should signal all just 
		created processes with the SIGTERM and, instead of printing the 
		message about creation, print out a message about interrupt of 
		the creation process. After that, the process should continue with wait()'temp2 loop as before.
		*/
		#ifdef WITH_SIGNALS
			if (interrupt_flag){
				printf("parent [%i]: Interrupt of the creation process!\n", parent_pid);
				kill(-2,SIGTERM);
				break;
			}
		#endif
	}
	
	for(;;)
	{
		temp1 = wait(&temp2);		
		if(temp1 == -1)		//No more signals to finish
			break; // end
		else
			{
			printf("child [%i]:[%i]: I've finished.\n",getppid(),temp1); // finished the process
			}
	}
	

	
	#ifdef WITH_SIGNALS // Signal handles need to be restored
		for(int j=0; j<NSIG;j++)
			sigaction(j,&sigrestore,NULL); // Setting signal for no usage of signal handler for signal j
	#endif


	return 0;
}
	
