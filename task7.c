#include <pthread.h>
#include <stdio.h>

#define N 5
#define LEFT (NUMBER_OF_PHILOSOPHERS+N-1)%N
#define RIGHT (NUMBER_OF_PHILOSOPHERS+1)%N
#define FOOD_LIMIT 1
#define EATING_TIME	1
#define THINKING_TIME 1
#define THINKING 1
#define EATING 2
#define NUMBER_OF_PHILOSOPHERS 5

pthread_mutex_t internal_mutex; 		
int state[N];			// THINKING(HUNGRY) or EATING(NOT HUNGRY IN A WHILE)
pthread_mutex_t philosophers_mutex[N];		
pthread_t philosophers[N];
char* list[5] = {"1","2","3","4","5"};

void grab_forks(int NUMBER_OF_PHILOSOPHERS);
void put_away_forks(int NUMBER_OF_PHILOSOPHERS);
void test(int NUMBER_OF_PHILOSOPHERS);
void lifecycle(int NUMBER_OF_PHILOSOPHERS);

int main()
{
	int NUMBER_OF_PHILOSOPHERS;
	pthread_mutex_init(&internal_mutex, NULL); // initialization of mutex with NULL
	for(NUMBER_OF_PHILOSOPHERS=0;NUMBER_OF_PHILOSOPHERS<N;++NUMBER_OF_PHILOSOPHERS)
    	{
		pthread_mutex_init(&philosophers_mutex[NUMBER_OF_PHILOSOPHERS], NULL);
    	}
    for(NUMBER_OF_PHILOSOPHERS=0;NUMBER_OF_PHILOSOPHERS<N;++NUMBER_OF_PHILOSOPHERS)
    	{
        pthread_mutex_lock(&philosophers_mutex[NUMBER_OF_PHILOSOPHERS]); // locking mutex
		pthread_create(&philosophers[NUMBER_OF_PHILOSOPHERS], NULL, lifecycle, NUMBER_OF_PHILOSOPHERS); // create a new thread
		}	
	for(NUMBER_OF_PHILOSOPHERS=0;NUMBER_OF_PHILOSOPHERS<N;++NUMBER_OF_PHILOSOPHERS)
    	{
		pthread_join(philosophers[NUMBER_OF_PHILOSOPHERS],NULL); // waitng other proccesses to being terminated
		}
	pthread_mutex_destroy(&internal_mutex);
	for(NUMBER_OF_PHILOSOPHERS=0;NUMBER_OF_PHILOSOPHERS<N;++NUMBER_OF_PHILOSOPHERS)
    	{
		pthread_mutex_destroy(&philosophers_mutex[NUMBER_OF_PHILOSOPHERS]); //destroying mutex and all of its attributes
		}
	pthread_exit(NULL);
	return 0;
}

void grab_forks(int NUMBER_OF_PHILOSOPHERS) 
{
	pthread_mutex_lock(&internal_mutex);
	state[NUMBER_OF_PHILOSOPHERS]=THINKING; // thinking
	test(NUMBER_OF_PHILOSOPHERS);
	pthread_mutex_unlock(&internal_mutex);
	pthread_mutex_lock(&philosophers_mutex[NUMBER_OF_PHILOSOPHERS]);
}

void put_away_forks(int NUMBER_OF_PHILOSOPHERS) 
{
	pthread_mutex_lock(&internal_mutex);
	state[NUMBER_OF_PHILOSOPHERS]=0;
    printf("%philosophers_mutex puts away left fork no. %d and right fork no. %d.\n",list[NUMBER_OF_PHILOSOPHERS],LEFT,NUMBER_OF_PHILOSOPHERS);
	test(LEFT);
	test(RIGHT);
	pthread_mutex_unlock(&internal_mutex);
}

void test(int NUMBER_OF_PHILOSOPHERS) 
{
	if(state[NUMBER_OF_PHILOSOPHERS] == THINKING && state[LEFT] !=EATING && state[RIGHT] != EATING)
		{
		state[NUMBER_OF_PHILOSOPHERS] = EATING;
		pthread_mutex_unlock(&philosophers_mutex[NUMBER_OF_PHILOSOPHERS]);
        printf("%philosophers_mutex is eating using left fork no. %d and right fork no. %d\n", list[NUMBER_OF_PHILOSOPHERS],LEFT,NUMBER_OF_PHILOSOPHERS);
		}
}

void lifecycle(int NUMBER_OF_PHILOSOPHERS)
{
	int meals_left=FOOD_LIMIT;
	printf("%philosophers_mutex came to the table.\n", list[NUMBER_OF_PHILOSOPHERS]);
	while(meals_left)
   	{
        printf("%philosophers_mutex is thinking.\n", list[NUMBER_OF_PHILOSOPHERS]);
		sleep(THINKING_TIME);
		grab_forks(NUMBER_OF_PHILOSOPHERS);
		sleep(EATING_TIME);
        printf("%philosophers_mutex finished his %d meal. \n", list[NUMBER_OF_PHILOSOPHERS],(FOOD_LIMIT-(--meals_left)));//--meals_left;
		put_away_forks(NUMBER_OF_PHILOSOPHERS);
        if(meals_left==0)
			printf ("%philosophers_mutex has left the table.\n",list[NUMBER_OF_PHILOSOPHERS]);
	}
}
